package com.travel.core.JitsVille;

public class GetPassengerType {
	public static Traveler getType(String travelerType) {
		if(travelerType.equalsIgnoreCase("Commutor")) {
			return new Commutor();
		}
		else if(travelerType.equalsIgnoreCase("Vacationer")) {
			return new Vacationer();
		}
		return null;
		
	}

}
