package com.travel.core.JitsVille;

public class Vacationer implements Traveler, Meals {

	
	public double ticketCost(double miles, int stops, boolean riderCard) {
		double ticketCost = 0;

		if (miles < 5) {
			throw new IllegalArgumentException("must travel for more than 5 miles atleast");
		} else if (miles > 4000) {
			throw new IllegalArgumentException("no of miles should be less than 4000");
		} else {
			ticketCost = miles * refactor;
		}
		return ticketCost;
	}

	public int getTotalMeals(double miles) {
		int mealsCount = 0;
		if(miles>100) {
		mealsCount = (int) Math.ceil(miles / 100);
		}
		return mealsCount;

	}

}
