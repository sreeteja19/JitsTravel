package com.travel.core.JitsVille;

import java.util.ArrayList;

public class TravelReport {

	int totalNewspaperCount = 0;
	int totaltripMeals = 0;
	int passengerMeals = 0;
	double cost = 0.0;
	double totalCost=0.0;
	
		 public double totalCost(ArrayList<Passenger> passengersList) {
				for (Passenger p : passengersList) {
					Traveler travaler = GetPassengerType.getType(p.getPassengerType());
					cost = travaler.ticketCost(p.getMiles(), p.getStops(), p.isFrequentCardRider());
					totalCost += cost;
				}
				return totalCost;
			}		 
	 public int totalMeals(ArrayList<Passenger> passengersList) {
		 
		 for (Passenger p : passengersList) {
			 Traveler travaler = GetPassengerType.getType(p.getPassengerType());
			 if (p.getPassengerType().equalsIgnoreCase("Vacationer")) {
					Meals meals = (Meals) travaler;
					passengerMeals = meals.getTotalMeals(p.getMiles());
					totaltripMeals += passengerMeals;
				}

		 }
		 return totaltripMeals;
	 }
	 public int totalnewsPaper(ArrayList<Passenger> passengersList) {
		 for (Passenger p : passengersList) {
			 if (p.isNewspaper()) {
					totalNewspaperCount++;
				}
		 }
		return totalNewspaperCount; 
	 }
}
