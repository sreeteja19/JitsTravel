package com.travel.core.JitsVille;

public class Passenger {
	private int id;
	private String name;
	private double miles;
	private int stops;
	private String passengerType;
	private final double refactor = 0.5;
	private boolean newspaper;
	private boolean frequentCardRider;

	public Passenger(int id, String name, double miles, int stops, String passengerType, boolean newspaper,
			boolean frequentCardRider) {
		this.id = id;
		this.name = name;
		this.miles = miles;
		this.stops = stops;
		this.setPassengerType(passengerType);
		this.newspaper = newspaper;
		this.frequentCardRider = frequentCardRider;

	}

	public boolean isFrequentCardRider() {
		return frequentCardRider;
	}

	public void setFrequentCardRider(boolean frequentCardRider) {
		this.frequentCardRider = frequentCardRider;
	}

	public boolean isNewspaper() {
		return newspaper;
	}

	public void setNewspaper(boolean newspaper) {
		this.newspaper = newspaper;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMiles() {
		return miles;
	}

	public int getStops() {
		return stops;
	}

	public String getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public double getRefactor() {
		return refactor;
	}

}
