package com.travel.core.JitsVille;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CommutorTest {

	Traveler traveler;
	Passenger passenger;
	@Before
	public void setUp() {
		traveler =new Commutor();
		passenger =new Passenger(112, "harry", 0, 5, "Commutor", true, true);
		
	}
	@Test
	public void commutorCostTest() {
	double expected=2.25;
	double actual=traveler.ticketCost(passenger.getMiles(), passenger.getStops(), passenger.isFrequentCardRider());
	assertEquals(expected, actual,0.01);

	}

}
