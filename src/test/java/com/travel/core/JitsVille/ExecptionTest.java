package com.travel.core.JitsVille;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ExecptionTest {
	Traveler traveler;
	Passenger passenger,passenger1;
	@Before
	public void setUp() {
		traveler =new Vacationer();
		passenger =new Passenger(111, "Tom", 3, 0, "Vacationer", true, false);
		passenger1 =new Passenger(111, "Tom", 4500, 0, "Vacationer", true, false);
		
	}
	@Rule public ExpectedException exe=ExpectedException.none();
	@Test
	public void testLeastMiles() {
		exe.expect(IllegalArgumentException.class);
		exe.expectMessage("must travel for more than 5 miles atleast");
		traveler.ticketCost(passenger.getMiles(), passenger.getStops(), passenger.isFrequentCardRider());
		
	}
	@Test
	public void testAtMostMiles() {
		exe.expect(IllegalArgumentException.class);
		exe.expectMessage("no of miles should be less than 4000");
		traveler.ticketCost(passenger1.getMiles(), passenger1.getStops(), passenger1.isFrequentCardRider());
		
	}

}
