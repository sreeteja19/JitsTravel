package com.travel.core.JitsVille;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ReportTest {
TravelReport report;
	ArrayList<Passenger> p = new ArrayList<Passenger>();

	@Before
	public void setUp() {
		report =new TravelReport();
		p.add(new Passenger(111, "Tom", 90, 0, "Vacationer", true, false));
		p.add(new Passenger(112, "Harry", 0, 5, "Commutor", true, true));
		p.add(new Passenger(113, "Jack", 0, 3, "commutor", false, true));
		p.add(new Passenger(114, "Rob", 199, 0, "Vacationer", true, false));
		p.add(new Passenger(114, "Steve", 0, 4, "commutor", true, false));
		
		
	}
	@Test
	public void testTotalMeals() {
		int expected=2;
		int actual=report.totalMeals(p);
		assertEquals(expected, actual);
		
	}
	@Test
	public void testTotalCost() {
		double expected=150.1;
		double actual=report.totalCost (p);
		assertEquals(expected, actual,0.01);
	}
	@Test
	public void testTotalNewsPaperCount()
	{
		int expected=4;
		int actual=report.totalnewsPaper(p);
		assertEquals(expected, actual);
		
	}

}
