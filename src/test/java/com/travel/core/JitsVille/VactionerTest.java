package com.travel.core.JitsVille;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VactionerTest {
	Traveler traveler;
	Passenger passenger;
	@Before
	public void setUp() {
		traveler =new Vacationer();
		passenger =new Passenger(111, "Tom", 90, 0, "Vacationer", true, false);
		
	}
	@Test
	public void vacationerCostTest() {
	double expected=45.0;
	double actual=traveler.ticketCost(passenger.getMiles(), passenger.getStops(), passenger.isFrequentCardRider());
	assertEquals(expected, actual,0.01);
	}
	@Test
	public void vacationerMealsTest() {
		int expected=0;
		int actual=0;
		assertEquals(expected, actual);
	}
	

}
